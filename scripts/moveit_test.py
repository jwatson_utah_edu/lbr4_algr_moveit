#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Template Version: 2016-06-25

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

"""
moveit_test.py , Built on Spyder for Python 2.7
James Watson, 2016 June
Testing LBR4 + Allegro with MoveIt! and RViz
Some code borrowed from: http://docs.ros.org/indigo/api/pr2_moveit_tutorials/html/planning/scripts/doc/move_group_python_interface_tutorial.html
"""

"""
== Run Notes ==
1. roscore
2. roslaunch lbr4_allegro_moveit demo.launch
3. cd /home/jwatson/regrasp_planning/src/lbr4_allegro_moveit/scripts
4. python moveit_test.py
"""

# == Init Environment ==================================================================================================
import sys, os.path
SOURCEDIR = os.path.dirname(os.path.abspath(__file__)) # URL, dir containing source file: http://stackoverflow.com/a/7783326

def add_first_valid_dir_to_path(dirList):
    """ Add the first valid directory in 'dirList' to the system path """
    # In lieu of actually installing the library, just keep a list of all the places it could be in each environment
    loadedOne = False
    for drctry in dirList:
        if os.path.exists( drctry ):
            sys.path.append( drctry )
            print 'Loaded', str(drctry)
            loadedOne = True
            break
    if not loadedOne:
        print "None of the specified directories were loaded"
# List all the places where the research environment could be
add_first_valid_dir_to_path( [ '/home/jwatson/regrasp_planning/researchenv',
                               '/media/jwatson/FILEPILE/Python/ResearchEnv' ] )
from ResearchEnv import * # Load the custom environment
from ResearchUtils.Vector import *
#import ResearchEnv.Vector.Vector

# == End Init ==========================================================================================================

# ~~ Imports ~~
#import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg


# == Helper Functions ==

def jnts_to_zero(grp):
    """ Set all the joints in a movement group to zero """
    jnt_values = grp.get_current_joint_values() # Then, we will get the current set of joint values for the group
    for index in range(len(jnt_values)):
        jnt_values[index] = 0
    grp.set_joint_value_target(jnt_values)  
    
def pose_from_vec_quat( frameStr , vec , quat ):
    """ Return a 'PoseStamped' relative to 'frameStr' at position 'vec' and orientation 'quat' """
    pose_target = geometry_msgs.msg.PoseStamped()
    pose_target.header.frame_id = frameStr
    pose_target.pose.position.x =  vec[0]
    pose_target.pose.position.y =  vec[1]
    pose_target.pose.position.z =  vec[2]
    pose_target.pose.orientation.x = quat.vctr[0] 
    pose_target.pose.orientation.y = quat.vctr[1] 
    pose_target.pose.orientation.z = quat.vctr[2] 
    pose_target.pose.orientation.w = quat.sclr 
    return pose_target

# == End Helper ==


# == Simulation Setup ==

#set_dbg_lvl(0) # Getting basic information from the sim
#set_dbg_lvl(1) # sending the robot to a pose
#set_dbg_lvl(2) # building a motion plan from waypoints
set_dbg_lvl(3) # trying to reach a target to grasp

dbgLog(-1, "Init comms with 'moveit_commander'")
moveit_commander.roscpp_initialize(sys.argv)

interfaceNodeName = 'lbr_allegro_MIC_interface' # Is this a topic or a node?
dbgLog(0, "Starting MoveIt! interface node:", interfaceNodeName)
rospy.init_node( interfaceNodeName , anonymous=True )
                
robot = moveit_commander.RobotCommander() #  This object is an interface to the robot as a whole.
scene = moveit_commander.PlanningSceneInterface() # This object is an interface to the world surrounding the robot.

# ~~~ MoveIt! must be running with a proper model in order for the following to work ~~~

grp_lbr4 = moveit_commander.MoveGroupCommander("LBR4")
grp_indx = moveit_commander.MoveGroupCommander("index_finger")
grp_thmb = moveit_commander.MoveGroupCommander("thumb")

# We create this DisplayTrajectory publisher which is used below to publish trajectories for RVIZ to visualize.
pathDisplayTopicName = '/move_group/display_planned_path'
dbgLog(0, "Starting planned path display topic:", pathDisplayTopicName)
display_trajectory_publisher = rospy.Publisher( pathDisplayTopicName , moveit_msgs.msg.DisplayTrajectory , queue_size=0 )

print SOURCEDIR

# == End Setup ==

if False: # Get simulation info
    # Fetch some basic info
    dbgLog(0, "LBR4 Reference frame: {}".format(grp_lbr4.get_planning_frame()) )  # /world
    dbgLog(0, "Index Reference frame: {}".format(grp_indx.get_planning_frame()) ) # /world
    dbgLog(0, "Thumb Reference frame: {}".format(grp_thmb.get_planning_frame()) ) # /world
    
    dbgLog(0, "LBR4 effector link: {}".format(grp_lbr4.get_end_effector_link()) )  # lbr4_7_link
    dbgLog(0, "Index effector link: {}".format(grp_indx.get_end_effector_link()) ) # index_tip
    dbgLog(0, "Thumb effector link: {}".format(grp_thmb.get_end_effector_link()) ) # thumb_tip
    
    dbgLog(0, "Robot group names:" )
    dbgLog(0, robot.get_group_names() ) # ['LBR4', 'index_finger', 'thumb']
    
    dbgLog(-1, "Robot current state:" )
    dbgLog(-1, robot.get_current_state() ) # gives a printout of all the joint states
    # TODO: Find out if 'get_current_state' outputs state in a structure that can be conveniently parsed. Is there a standard?

if False: # Generating moves from a desired pose and manually
    dbgLog(1, endl, sep("Generate a pose for the robot",strOut=True) )
    pose_target = geometry_msgs.msg.Pose() # Instantiate a pose to be fed to the planner
    pose_target.position.x = 0.5 # set the target effector X coord # TODO: encapsulate all these calls into a single function
    pose_target.position.y = 0.5 # set the target effector Y coord
    pose_target.position.z = 0.5 # set the target effector Z coord
    grp_lbr4.set_pose_target(pose_target)
    
    plan1 = grp_lbr4.plan() # Displays a plan!
    dbgLog(1, "Waiting while RVIZ displays plan1..." )
    rospy.sleep(5)
    
    """ You can ask RVIZ to visualize a plan (aka trajectory) for you. But the group.plan() method does this automatically so 
    this is not that useful here (it just displays the same trajectory again). """
    
    #print "============ Visualizing plan1"
    #display_trajectory = moveit_msgs.msg.DisplayTrajectory()
    #
    #display_trajectory.trajectory_start = robot.get_current_state()
    #display_trajectory.trajectory.append(plan1)
    #display_trajectory_publisher.publish(display_trajectory);
    #
    #print "============ Waiting while plan1 is visualized (again)..."
    #rospy.sleep(5)
    
    grp_lbr4.clear_pose_targets() # First, we will clear the pose target we had just set.
    lbr4_variable_values = grp_lbr4.get_current_joint_values() # Then, we will get the current set of joint values for the group
    lbr4_variable_values[0] = 1.0
    grp_lbr4.set_joint_value_target(lbr4_variable_values)
    plan2 = grp_lbr4.plan() # Rotation of Joint 1 added to the plan
    dbgLog(1, "Waiting while RVIZ displays plan2..." )
    rospy.sleep(5)

if False: # Adding objects to the environment
    # URL, Creating a scene with the Python MoveIt! API: https://github.com/guihomework/dexterous-manipulation-tutorial/wiki/tuto-using-part1
    scene = moveit_commander.PlanningSceneInterface()
    rospy.sleep(2) # Maybe rospy needs to take a breather before adding something?
    # URL, sleep before add: http://answers.ros.org/question/209030/moveit-planningsceneinterface-addbox-not-showing-in-rviz/
    
    p = geometry_msgs.msg.PoseStamped()
    p.header.frame_id = robot.get_planning_frame() #"/world"
    p.pose.position.x =  0.0 # THIS IS A GOOD PLAN KEEP IT FOR NOW! (0.5,-0.5), (0.0,0.5)
    p.pose.position.y =  0.5
    p.pose.position.z =  0.03
    p.pose.orientation.x = 0.0
    p.pose.orientation.y = 0.0
    p.pose.orientation.z = 1.0
    p.pose.orientation.w = 1.0  
    meshPath = "/home/jwatson/regrasp_planning/src/lbr4_allegro_moveit/meshes/ycb/bowl.stl"
    scene.add_mesh("bowl",p,meshPath) # fetch the bowl from the path and place it in the specified pose

if False:
    grp_lbr4.clear_pose_targets() # First, we will clear the pose target we had just set.  
    grp_lbr4.allow_replanning(True) # NOT SURE IF THIS WILL HELP!
    jnts_to_zero(grp_lbr4)    
    # Define a quaternion that rotates the world frame to the desired frame
    poseQuat = Quaternion.k_rot_to_Quat([0.0,1.0,0.0], -pi) # point the link down I guess
    #poseQuat = Quaternion.k_rot_to_Quat([0.0,0.0,1.0], pi/2) # point the link down I guess
    #poseQuat = Quaternion.compose_rots(  )
    dbgLog(3, poseQuat ) 
    
    # IF I USE A STAMPED POSE WILL IT BE WITH RESPECT TO THE BASE FRAME? - If I define it to be, yes
    #pose_target = geometry_msgs.msg.Pose() # Instantiate a pose to be fed to the planner
    pose_target = geometry_msgs.msg.PoseStamped()
    pose_target.header.frame_id = robot.get_planning_frame() #"/world"
    pose_target.pose.position.x =  0.0 # set the target effector X coord # TODO: encapsulate all these calls into a single function
    pose_target.pose.position.y =  0.5 # set the target effector Y coord
    pose_target.pose.position.z =  0.4 # set the target effector Z coord
    pose_target.pose.orientation.x = poseQuat.vctr[0] # 1 #-waypoints[0].orientation.x # pointing down I suppose?
    pose_target.pose.orientation.y = poseQuat.vctr[1] # 1 #-waypoints[0].orientation.y
    pose_target.pose.orientation.z = poseQuat.vctr[2] # -1 #-waypoints[0].orientation.z
    pose_target.pose.orientation.w = poseQuat.sclr #1 #waypoints[0].orientation.w
    grp_lbr4.set_pose_target(pose_target)
    
    plan1 = grp_lbr4.plan() # Displays a plan!
    dbgLog(3, "Generated a plan:", endl,plan1 )
    dbgLog(3, "Waiting while RVIZ displays plan1..." )
    rospy.sleep(2)

if True: # Adding objects to the environment
    # URL, Creating a scene with the Python MoveIt! API: https://github.com/guihomework/dexterous-manipulation-tutorial/wiki/tuto-using-part1
    scene = moveit_commander.PlanningSceneInterface()
    rospy.sleep(2) # Maybe rospy needs to take a breather before adding something?
    # URL, sleep before add: http://answers.ros.org/question/209030/moveit-planningsceneinterface-addbox-not-showing-in-rviz/
    xTarget = -0.6
    yTarget =  0.6
    targetPos = [ xTarget , yTarget , 0.4]
    plmOgnPos = [ xTarget + 0.1 , yTarget - 0.1 , 0.5]
    poseQuat = Quaternion.k_rot_to_Quat([0.0,0.0,1.0], 1) # No rotation
    #print robot.get_planning_frame()
    p = pose_from_vec_quat( robot.get_planning_frame() , targetPos , poseQuat )
    scene.add_sphere('target', p, 0.03)
    p = pose_from_vec_quat( robot.get_planning_frame() , plmOgnPos , poseQuat )
    scene.add_sphere('palm_origin', p, 0.03)
    tableHeight = 0.3
    p = pose_from_vec_quat( robot.get_planning_frame() , [ xTarget , yTarget , tableHeight/2.0] , poseQuat )
    scene.add_box('table', p, (1.0 , 1.0 , tableHeight))
    meshPath = "/home/jwatson/regrasp_planning/src/lbr4_allegro_moveit/meshes/ycb/bowl.stl"
    p = pose_from_vec_quat( robot.get_planning_frame() , [ xTarget , yTarget , tableHeight] , poseQuat )
    scene.add_mesh("bowl",p,meshPath) # fetch the bowl from the path and place it in the specified pose

if True:
    grp_lbr4.clear_pose_targets() # First, we will clear the pose target we had just set.  
    grp_lbr4.allow_replanning(True) # NOT SURE IF THIS WILL HELP!
    
    jnts_to_zero(grp_lbr4)    
    
    zPalm = vec_unit( np.subtract( targetPos , plmOgnPos ) ) # In the future this will be parallel to the grasped cluster
    yPalm = vec_unit( np.cross( [0.0 , 0.0 , 1.0] , zPalm ) ) # In the future this will be parallel to the grasped cluster
    xPalm = np.cross( yPalm , zPalm ) # In the future this will be parallel to the cluster normal
    
    print check_orthonormal( xPalm , yPalm , zPalm )    
    
    grp_lbr4.set_pose_target( pose_from_vec_quat( robot.get_planning_frame() , 
                                                  plmOgnPos , 
                                                  Quaternion.principal_rot_Quat( xPalm , yPalm , zPalm ) ) )
    plan1 = grp_lbr4.plan() 
    dbgLog(3, "Generated a plan:", endl,plan1 )
    dbgLog(3, "Waiting while RVIZ displays plan1..." )
    rospy.sleep(2)
    print robot.get_current_state()
    
moveit_commander.os._exit(0) # MoveIt! will throw an error if this line is not included
# URL, MoveIt! weird boost errors when Python script exits: https://github.com/ros-planning/moveit_ros/issues/392

# == Abandoned Code ==

## Create a pose where to put objects, using the world as reference
#    p = geometry_msgs.msg.PoseStamped()
#    p.header.frame_id = robot.get_planning_frame() #"/world"
#    p.pose.position.x = 0
#    p.pose.position.y = 0
#    p.pose.position.z = -0.05
#    p.pose.orientation.x = 0
#    p.pose.orientation.y = 0
#    p.pose.orientation.z = 0
#    p.pose.orientation.w = 1
#
#    scene.add_box("ground", p, (3, 3, 0.02))

# == End Abandoned ==